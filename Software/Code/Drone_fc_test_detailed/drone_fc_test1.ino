
//////////////////////////////Include libraries//////////////////////////////////////////////////

#include <Servo.h>                  //Using Servo.h library to control ESC.
#include <Wire.h>                   //Using Wire.h library so we can communicate with the gyro.
#include "util.h"
#include "pid.h"
#include "gyro.h"


/////////////////////////////////////////////////////////////////////////////////////////
//////////pins 5 6 10 11 correspond to motor no. 1(CCW) 2(CW) 3(CCW) 4(CW) respectively
//////////p q r s represent motors 1 2 3 4 repectively
/////////////////////////////////////////////////////////////////////////////////////////


char data1;                         //Variable for storing received data from HC 05
int p = 0, q = 0;
int r = 0, s = 0, lol=0;            //Variables for motors
int throttle = 0, thro_map = 0;     //Variable for mapping throttle and standardised throttle variables
int val1, val2, val3, val4;         //Creating 'val' variables for mapping motors
int esc_1, esc_2, esc_3, esc_4;     //Variables for ESC pulses in active control

Servo esc1;                         //Creating a servo class with name as esc for all motors
Servo esc2;
Servo esc3;
Servo esc4;

// program state
enum State { STOPPED, STARTING, STARTED, LANDING };
State state = STOPPED;

unsigned long loop_timer = 0;       // loop timer

struct Gyroscope gyro;              // gyroscope
struct PID pid;                     // PID settings

double pitch_angle, roll_angle;
double pitch_acc = 0, roll_acc = 0;
double landing_throttle;

void setup() {
  // zero data structures
  memset(&gyro, 0, sizeof(gyro));
  memset(&pid, 0, sizeof(pid));

Serial.begin(9600);
Wire.begin();                              //Start the I2C as master.


esc1.attach(5);                            //Specify the esc signal pins
esc2.attach(6);
esc3.attach(10);
esc4.attach(11);
esc1.writeMicroseconds(1000);              //initialize the signal to 1000
esc2.writeMicroseconds(1000);
esc3.writeMicroseconds(1000);
esc4.writeMicroseconds(1000);

  // start the gyroscope
  gyro.address = GYRO_ADDR;
  enableGyro(&gyro);

  // read calibration values from gyroscope
  while (!readGyroFromEEPROM(gyro, GYRO_STRUCT_LOC)) {
    delayMicroseconds(250);
  }

  // setup PID settings
  setupPID(&pid);

  loop_timer = micros();
}

void loop() {
///////////////////////////////////////////////////////////////////////////////////////
////////////////////////Bluetooth Control (Passive)////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

  if(Serial.available() > 0){
   data1 = Serial.read();
   if(data1 == 'U'){        //U = Up
    p++;
    q++;
    r++;
    s++;
    lol++;
    thro_map++;
    Serial.print("The throttle map is:  ");
    Serial.print(thro_map);
   }
   if(data1 == 'D'){        //D = Down
    p--;
    q--;
    r--;
    s--;
    lol--;
    thro_map--;
    Serial.print("The throttle map is:  ");
    Serial.print(thro_map);
   }
   if(data1 == 'F'){        //F = Front
    q++;
    r++;}
   if(data1 == 'B'){        //B = Back
    p++;
    s++;}
   if(data1 == 'L'){        //L = Left
    p++;
    q++;}
   if(data1 == 'R'){        //R = Right
    r++;
    s++;}
   if(data1 == 'E'){        //F = Front
    lol = 0;
    p = 0;
    q = 0;
    r = 0;
    s = 0;
    setState(STOPPED);
   }
}

throttle = 1000 + (thro_map*100);    //Standardising throttle variable

   if(lol > 10){
   p = 10;
   q = 10;
   r = 10;
   s = 10;}
   if(lol < 0){
   p = 0;
   q = 0;
   r = 0;
   s = 0;}

val1 = p;
val2 = q;
val3 = r;
val4 = s;

val1 = map(val1, 0, 10,1000,2000);  //mapping motor1 throttle to minimum and maximum(Change if needed) 
val2 = map(val2, 0, 10,1000,2000);  //mapping motor2 throttle to minimum and maximum(Change if needed) 
val3 = map(val3, 0, 10,1000,2000);  //mapping motor3 throttle to minimum and maximum(Change if needed) 
val4 = map(val4, 0, 10,1000,2000);  //mapping motor4 throttle to minimum and maximum(Change if needed) 


esc1.writeMicroseconds(val1);       //giving the signal to esc
esc2.writeMicroseconds(val2);
esc3.writeMicroseconds(val3); 
esc4.writeMicroseconds(val4);


if(data1 == 'F'){
    delay(300);
    q--;
    r--;
    val2 = q;
    val3 = r;
    val2 = map(val2, 0, 10,1000,2000); 
    val3 = map(val3, 0, 10,1000,2000);
    esc2.writeMicroseconds(val2);
    esc3.writeMicroseconds(val3);
   }
if(data1 == 'B'){
    delay(300);
    p--;
    s--;
    val1 = p;
    val4 = s;
    val1 = map(val1, 0, 10,1000,2000);
    val4 = map(val4, 0, 10,1000,2000);
    esc1.writeMicroseconds(val1);
    esc4.writeMicroseconds(val4);
   }
if(data1 == 'L'){
    delay(300);
    p--;
    q--;
    val1 = p;
    val2 = q;
    val1 = map(val1, 0, 10,1000,2000); 
    val2 = map(val2, 0, 10,1000,2000);
    esc1.writeMicroseconds(val1);
    esc2.writeMicroseconds(val2);
}
if(data1 == 'R'){
    delay(300);
    r--;
    s--;
    val3 = r;
    val4 = s;
    val3 = map(val3, 0, 10,1000,2000);
    val4 = map(val4, 0, 10,1000,2000);
    esc3.writeMicroseconds(val3); 
    esc4.writeMicroseconds(val4);
}
data1='Z';

/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////Active Control///////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

////////////// calculating ESC pulses necessary to level/////////////////////////////

  // calculate current orientation according to the gyro
  // use a 70-30 complementary filter
  // 65.5 is from the MPU-6050 spec

    Serial.print("gyro values are: ");
    Serial.print(gyro.yaw);
    Serial.print(gyro.roll);
    Serial.print(gyro.pitch);

  pid.yaw.gyro = (pid.yaw.gyro * 0.7) + ((gyro.yaw / 65.5) * 0.3);
  pid.roll.gyro = (pid.roll.gyro * 0.7) + ((gyro.roll / 65.5) * 0.3);
  pid.pitch.gyro = (pid.pitch.gyro * 0.7) + ((gyro.pitch / 65.5) * 0.3);

    Serial.print("pid YRP values:  ");
    Serial.print(pid.yaw.gyro);
    Serial.print(pid.roll.gyro);
    Serial.print(pid.pitch.gyro);

  // calculate the angles
  // 0.0000611 = 1 / 250Hz / 65.5
  pitch_angle += gyro.pitch * 0.0000611;
  roll_angle += gyro.roll * 0.0000611;

  // shift the roll/pitch axes if the drone has yawed
  pitch_angle -= roll_angle * sin(gyro.yaw * 0.000001066);
  roll_angle += pitch_angle * sin(gyro.yaw * 0.000001066);

  // calculate the total acceleration
  double total_acc = sqrt(pow2(gyro.acc.x) + pow2(gyro.acc.y) + pow2(gyro.acc.z));
  if (abs(gyro.acc.y) < total_acc)
    pitch_acc = asin((float)gyro.acc.y/total_acc) * 57.296;
  if (abs(gyro.acc.x) < total_acc)
    roll_acc = -asin((float)gyro.acc.x/total_acc) * 57.296;

  // use acceleration to correct the drift from the gyro
  roll_angle = (roll_angle * 0.9996) + (roll_acc * 0.0004);
  pitch_angle = (pitch_angle * 0.9996) + (pitch_acc * 0.0004);

    Serial.print("Angles are: ");
    Serial.print(roll_angle);
    Serial.print(pitch_angle);

  double roll_adjust = roll_angle * 17.0;
  double pitch_adjust = pitch_angle * 17.0;

  // set PID targets
  pid.roll.target = 0;
  pid.roll.target -= roll_adjust;
  pid.roll.target /= 3;             // convert to degrees
    
    Serial.print("setpoints: ");
    Serial.print(pid.roll.target);
    
  pid.pitch.target = 0;
  pid.pitch.target -= pitch_adjust;
  pid.pitch.target /= 3;

    Serial.print(pid.pitch.target);
  
  pid.yaw.target = 0;
  
  // based on program state, do different things
  switch (state) {
  case STOPPED: {
    if (lol >= 2)setState(STARTING);
    resetESCPulses();
        Serial.print("switched to STARTING MODE");
    break;
  }

  case STARTING: {
    // do pre-flight setup
    pid.yaw.i.total = 0;
    pid.roll.i.total = 0;
    pid.pitch.i.total = 0;

    pid.yaw.d.prev = 0;
    pid.roll.d.prev = 0;
    pid.pitch.d.prev = 0;

    roll_angle = roll_acc;
    pitch_angle = pitch_acc;
    
    setState(STARTED);
    Serial.print("switched to STARTED MODE");
    resetESCPulses();
    break;
  }

  case STARTED: {
    calculatePID(&pid);
    calculateESCPulses(throttle, &pid);
    if (throttle < 1200){setState(STOPPED);
    Serial.print("switched to STOPPED MODE");
}
    break;
  }
  }

 /* // keep loop at 4000 microseconds (250 Hz) so ESC can function properly
  while (micros() - loop_timer < 4000);
  loop_timer = micros();

  PORTD |= 0b11110000; // turn on all the ESC
  unsigned long timer_1 = esc_fr + loop_timer;
  unsigned long timer_2 = esc_rr + loop_timer;
  unsigned long timer_3 = esc_rl + loop_timer;
  unsigned long timer_4 = esc_fl + loop_timer;
*/
  // there is always at least 1ms of free time, do something useful
  readGyroValues(&gyro);

 /* while (PORTD & 0b11110000) {
    register unsigned long t = micros();
    if (timer_1 <= t) PORTD &= 0b11101111;
    if (timer_2 <= t) PORTD &= 0b11011111;
    if (timer_3 <= t) PORTD &= 0b10111111;
    if (timer_4 <= t) PORTD &= 0b01111111;
  }*/
}

inline void setState(State s) {
  state = s;}

inline void resetESCPulses() {
  esc_1 = 1000;
  esc_2 = 1000;
  esc_3 = 1000;
  esc_4 = 1000;}

inline void calculateESCPulses(int throttle, struct PID* pid) {
  // calculate ESC pulses necessary to level
    throttle = min(1800, throttle);                                                //Restricting the throttle to 1800us pulse
    esc_1 = throttle - pid->pitch.output + pid->roll.output - pid->yaw.output;
    esc_2 = throttle + pid->pitch.output + pid->roll.output + pid->yaw.output;
    esc_3 = throttle + pid->pitch.output - pid->roll.output - pid->yaw.output;
    esc_4 = throttle - pid->pitch.output - pid->roll.output + pid->yaw.output;

    if(esc_1 > 2000)esc_1 = 2000;                          //Limit the esc-1 pulse to 2000us.
    if(esc_2 > 2000)esc_2 = 2000;                          //Limit the esc-2 pulse to 2000us.
    if(esc_3 > 2000)esc_3 = 2000;                          //Limit the esc-3 pulse to 2000us.
    if(esc_4 > 2000)esc_4 = 2000;                          //Limit the esc-4 pulse to 2000us.

    if(esc_1 < 1100)esc_1 = 1100;                          //Limit the esc-1 pulse to 2000us.
    if(esc_2 < 1100)esc_2 = 1100;                          //Limit the esc-2 pulse to 2000us.
    if(esc_3 < 1100)esc_3 = 1100;                          //Limit the esc-3 pulse to 2000us.
    if(esc_4 < 1100)esc_4 = 1100;                          //Limit the esc-4 pulse to 2000us.    

    Serial.print("Calculated ESC pulses are:  ");
    Serial.print(esc_1);
    Serial.print(esc_2);
    Serial.print(esc_3);
    Serial.print(esc_4);

    esc1.writeMicroseconds(esc_1);                                                  //giving the signal to ESCs
    esc2.writeMicroseconds(esc_2);
    esc3.writeMicroseconds(esc_3); 
    esc4.writeMicroseconds(esc_4);
}
