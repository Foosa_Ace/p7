#ifndef _UTIL_H
#define _UTIL_H

#ifndef max
#define max(A,B) ({ \
        typeof (A) _a = (A); \
        typeof (B) _b = (B); \
        (_a > _b) ? _a : _b; \
      })
#endif

#ifndef min
#define min(A,B) ({ \
        typeof (A) _a = (A); \
        typeof (B) _b = (B); \
        (_a < _b) ? _a : _b; \
      })
#endif

#ifndef pow2
#define pow2(A) ((A) * (A))
#endif

#ifndef limit
#define limit(low,v,high)   min((high), max((low), (v)))
#endif


const int GYRO_CALIBRATED_LOC = 300;
const int GYRO_STRUCT_LOC = 301;

#endif
