///////////////////Global #define required for pid.h file/////////////////////////
#ifndef max
#define max(A,B) ({ \
        typeof (A) _a = (A); \
        typeof (B) _b = (B); \
        (_a > _b) ? _a : _b; \
      })
#endif

#ifndef min
#define min(A,B) ({ \
        typeof (A) _a = (A); \
        typeof (B) _b = (B); \
        (_a < _b) ? _a : _b; \
      })
#endif

#ifndef pow2
#define pow2(A) ((A) * (A))
#endif

#ifndef limit
#define limit(low,v,high)   min((high), max((low), (v)))
#endif

////////////////////Include libraries//////////////////////////////////////////////////

#include <Servo.h>               //Using Servo.h library to control ESC.
#include <Wire.h>                //Using Wire.h library so we can communicate with the gyro.
#include <pid.h>                 //Using pid.h library to include digital PID control.


/////////////////////////////////////////////////////////////////////////////////////////
//pins 5 6 10 11 correspond to motor no. 1(CCW) 2(CW) 3(CCW) 4(CW) respectively
//p q r s represent motors 1 2 3 4 repectively
/////////////////////////////////////////////////////////////////////////////////////////


char data1;                     //Variable for storing received data from HC 05
int p = 0, q = 0;
int r = 0, s = 0, lol=0;        //Variables for motors
int throttle = 0, thro_map = 0; //Variable for mapping throttle and standardised throttle variables
int val1, val2, val3, val4;     //Creating 'val' variables for mapping motors
int esc_1, esc_2, esc_3, esc_4; //Variables for ESC pulses in active control

Servo esc1;                     //Creating a servo class with name as esc for all motors
Servo esc2;
Servo esc3;
Servo esc4;

struct PID pid;                     // PID settings


void setup() {

digitalWrite(12, HIGH);

Wire.begin();                              //Start the I2C as master.
    
esc1.attach(5);                            //Specify the esc signal pins
esc2.attach(6);
esc3.attach(10);
esc4.attach(11);
esc1.writeMicroseconds(1000);              //initialize the signal to 1000
esc2.writeMicroseconds(1000);
esc3.writeMicroseconds(1000);
esc4.writeMicroseconds(1000);

memset(&pid, 0, sizeof(pid));              //Change all attributes in pid.h to '0'
setupPID(&pid);                            //Setup the pid (check pid.h)

Serial.begin(9600);

digitalWrite(12, LOW);

}

void loop() {
    
///////////////////////////////////////////////////////////////////////////////////////
////////////////////////Bluetooth Control (Passive)////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

  if(Serial.available() > 0){
   data1 = Serial.read();
   if(data1 == 'U'){        //U = Up
    p++;
    q++;
    r++;
    s++;
    lol++;
    thro_map++;
   }
   if(data1 == 'D'){        //D = Down
    p--;
    q--;
    r--;
    s--;
    lol--;
    thro_map--;
   }
   if(data1 == 'F'){        //F = Front
    q++;
    r++;
   }
   if(data1 == 'B'){        //B = Back
    p++;
    s++;}
   if(data1 == 'L'){        //L = Left
    p++;
    q++;}
   if(data1 == 'R'){        //R = Right
    r++;
    s++;}
}

throttle = thro_map*100;    //Standardising throttle variable

   if(lol > 10){
   p = 10;
   q = 10;
   r = 10;
   s = 10;}
   if(lol < 0){
   p = 0;
   q = 0;
   r = 0;
   s = 0;}

val1 = p;
val2 = q;
val3 = r;
val4 = s;

val1 = map(val1, 0, 10,1000,2000);  //mapping motor1 throttle to minimum and maximum(Change if needed) 
val2 = map(val2, 0, 10,1000,2000);  //mapping motor2 throttle to minimum and maximum(Change if needed) 
val3 = map(val3, 0, 10,1000,2000);  //mapping motor3 throttle to minimum and maximum(Change if needed) 
val4 = map(val4, 0, 10,1000,2000);  //mapping motor4 throttle to minimum and maximum(Change if needed) 


esc1.writeMicroseconds(val1);   //giving the signal to esc
esc2.writeMicroseconds(val2);
esc3.writeMicroseconds(val3); 
esc4.writeMicroseconds(val4);


if(data1 == 'F'){
    delay(400);
    q--;
    r--;
    val2 = q;
    val3 = r;
    val2 = map(val2, 0, 10,1000,2000); 
    val3 = map(val3, 0, 10,1000,2000);
    esc2.writeMicroseconds(val2);
    esc3.writeMicroseconds(val3);
   }
if(data1 == 'B'){
    delay(400);
    p--;
    s--;
    val1 = p;
    val4 = s;
    val1 = map(val1, 0, 10,1000,2000);
    val4 = map(val4, 0, 10,1000,2000);
    esc1.writeMicroseconds(val1);
    esc4.writeMicroseconds(val4);
   }
if(data1 == 'L'){
    delay(400);
    p--;
    q--;
    val1 = p;
    val2 = q;
    val1 = map(val1, 0, 10,1000,2000); 
    val2 = map(val2, 0, 10,1000,2000);
    esc1.writeMicroseconds(val1);
    esc2.writeMicroseconds(val2);
}
if(data1 == 'R'){
    delay(400);
    r--;
    s--;
    val3 = r;
    val4 = s;
    val3 = map(val3, 0, 10,1000,2000);
    val4 = map(val4, 0, 10,1000,2000);
    esc3.writeMicroseconds(val3); 
    esc4.writeMicroseconds(val4);
}
data1='Z';

/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////Active Control///////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

////////////// calculating ESC pulses necessary to level/////////////////////////////

    calculatePID(&pid);                            //Calculating the PID stuff

    throttle = min(1800, throttle);                //Restricting the throttle to 1800us pulse
    esc_1 = throttle - pid->pitch.output + pid->roll.output - pid->yaw.output;
    esc_2 = throttle + pid->pitch.output + pid->roll.output + pid->yaw.output;
    esc_3 = throttle + pid->pitch.output - pid->roll.output - pid->yaw.output;
    esc_4 = throttle - pid->pitch.output - pid->roll.output + pid->yaw.output;
    
    if(esc_1 > 2000)esc_1 = 2000;                          //Limit the esc-1 pulse to 2000us.
    if(esc_2 > 2000)esc_2 = 2000;                          //Limit the esc-2 pulse to 2000us.
    if(esc_3 > 2000)esc_3 = 2000;                          //Limit the esc-3 pulse to 2000us.
    if(esc_4 > 2000)esc_4 = 2000;                          //Limit the esc-4 pulse to 2000us.

    if(esc_1 < 1100)esc_1 = 1100;                          //Limit the esc-1 pulse to 2000us.
    if(esc_2 < 1100)esc_2 = 1100;                          //Limit the esc-2 pulse to 2000us.
    if(esc_3 < 1100)esc_3 = 1100;                          //Limit the esc-3 pulse to 2000us.
    if(esc_4 < 1100)esc_4 = 1100;                          //Limit the esc-4 pulse to 2000us.    

    esc1.writeMicroseconds(esc_1);                         //giving the signal to ESCs
    esc2.writeMicroseconds(esc_2);
    esc3.writeMicroseconds(esc_3); 
    esc4.writeMicroseconds(esc_4);
}
