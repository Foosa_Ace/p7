//////////////////////////////Include libraries//////////////////////////////////////////////////

#include <Servo.h>                  //Using Servo.h library to control ESC.
#include <Wire.h>                   //Using Wire.h library so we can communicate with the gyro.
#include "util.h"
#include "pid.h"
//#include "gyro.h"
/////////////////////////////////////////////////////////////////////////////////////////////////
#include "I2Cdev.h"

#include "MPU6050_6Axis_MotionApps20.h"
//#include "MPU6050.h" // not necessary if using MotionApps include file

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

#define INTERRUPT_PIN 2  // use pin 2 on Arduino Uno & most boards
#define LED_PIN 13 // (Arduino is 13, Teensy is 11, Teensy++ is 6)
bool blinkState = false;

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

float ax, ay, az;

MPU6050 mpu;

////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////
//////////pins 5 6 10 11 correspond to motor no. 1(CCW) 2(CW) 3(CCW) 4(CW) respectively
//////////p q r s represent motors 1 2 3 4 repectively
/////////////////////////////////////////////////////////////////////////////////////////

float y, p, r;

char data1;                         //Variable for storing received data from HC 05
int p1 = 0, q1 = 0;                   //p = p1, q = q1 for now
int r1 = 0, s1 = 0, lol=0;            //Variables for motors
int throttle = 0, thro_map = 0;     //Variable for mapping throttle and standardised throttle variables
int val1, val2, val3, val4;         //Creating 'val' variables for mapping motors
int esc_1, esc_2, esc_3, esc_4;     //Variables for ESC pulses in active control

Servo esc1;                         //Creating a servo class with name as esc for all motors
Servo esc2;
Servo esc3;
Servo esc4;

// program state
enum State { STOPPED, STARTING, STARTED, LANDING };
State state = STOPPED;

unsigned long loop_timer = 0;       // loop timer

//struct Gyroscope gyro;              // gyroscope
struct PID pid;                       // PID settings

double pitch_angle, roll_angle;
double pitch_acc = 0, roll_acc = 0;
double landing_throttle;


volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
} 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
void setup() {

// join I2C bus (I2Cdev library doesn't do this automatically)
//    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    Wire.begin();
    Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
    
    // initialize device
    Serial.println(F("Initializing I2C devices..."));
    mpu.initialize();
    pinMode(INTERRUPT_PIN, INPUT);

    // verify connection
    Serial.println(F("Testing device connections..."));
    Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

    // wait for ready
    Serial.println(F("\nSend any character to begin DMP programming and demo: "));
    //while (Serial.available() && Serial.read()); // empty buffer
    //while (!Serial.available());                 // wait for data
    //while (Serial.available() && Serial.read()); // empty buffer again

    // load and configure the DMP
    Serial.println(F("Initializing DMP..."));
    devStatus = mpu.dmpInitialize();
    

    // make sure it worked (returns 0 if so)
    if (devStatus == 0) {
        // turn on the DMP, now that it's ready
        Serial.println(F("Enabling DMP..."));
        mpu.setDMPEnabled(true);

        // enable Arduino interrupt detection
        Serial.print(F("Enabling interrupt detection (Arduino external interrupt "));
        Serial.print(digitalPinToInterrupt(INTERRUPT_PIN));
        Serial.println(F(")..."));
        attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
        mpuIntStatus = mpu.getIntStatus();

        // set our DMP Ready flag so the main loop() function knows it's okay to use it
        Serial.println(F("DMP ready! Waiting for first interrupt..."));
        dmpReady = true;

        // get expected DMP packet size for later comparison
        packetSize = mpu.dmpGetFIFOPacketSize();
        } 
        else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        Serial.print(F("DMP Initialization failed (code "));
        Serial.print(devStatus);
        Serial.println(F(")"));
    }

    // configure LED for output
    pinMode(LED_PIN, OUTPUT);


/////////////////////////////////////////////////////////////////////////////////////////////


   Serial.println("////////////////////SETUP//////////////////");

   
    mpu.setXGyroOffset(113);
    mpu.setYGyroOffset(-58);
    mpu.setZGyroOffset(0);
    mpu.setZAccelOffset(1220); 

  // zero data structures
//  memset(&gyro, 0, sizeof(gyro));
  memset(&pid, 0, sizeof(pid));

Serial.begin(9600);
Wire.begin();                              //Start the I2C as master.


esc1.attach(5);                            //Specify the esc signal pins
esc2.attach(6);
esc3.attach(10);
esc4.attach(11);
esc1.writeMicroseconds(1000);              //initialize the signal to 1000
esc2.writeMicroseconds(1000);
esc3.writeMicroseconds(1000);
esc4.writeMicroseconds(1000);

// start the gyroscope
//  gyro.address = GYRO_ADDR;
//  enableGyro(&gyro);

  // read calibration values from gyroscope
//  while (!readGyroFromEEPROM(gyro, GYRO_STRUCT_LOC)) {
//    delayMicroseconds(250);
//}

  // setup PID settings
  setupPID(&pid);

 // loop_timer = micros();

}

void loop() {
//////////////////////////////////////////////////////////////////////////////////////

    // wait for MPU interrupt or extra packet(s) available
     while (!mpuInterrupt && fifoCount < packetSize) {
        if (mpuInterrupt && fifoCount < packetSize) {
          // try to get out of the infinite loop 
          fifoCount = mpu.getFIFOCount();
        }
     }  


    if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024) {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        fifoCount = mpu.getFIFOCount();
        //Serial.println(F("FIFO overflow!"));
    }
    
    // otherwise, check for DMP data ready interrupt (this should happen frequently)
     else if (mpuIntStatus & _BV(MPU6050_INTERRUPT_DMP_INT_BIT)) {
        // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
     }

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;

         // display Euler angles in degrees
            mpu.dmpGetQuaternion(&q, fifoBuffer);
            mpu.dmpGetAccel(&aa, fifoBuffer);
            mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
            mpu.dmpGetLinearAccelInWorld(&aaWorld, &aaReal, &q);

            mpu.dmpGetQuaternion(&q, fifoBuffer);
            mpu.dmpGetGravity(&gravity, &q);
            mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
 /*           
            Serial.print("euler from DMP\t");
            Serial.print(ypr[0] * 180/M_PI);
            Serial.print("\t");
            Serial.print(ypr[1] * 180/M_PI);
            Serial.print("\t");
            Serial.println(ypr[2] * 180/M_PI);
*/
            y = ypr[0] * 180/M_PI;
            p = ypr[1] * 180/M_PI;
            r = ypr[2] * 180/M_PI;

            ax = aaWorld.x;
            ay = aaWorld.y;
            az = aaWorld.z;

///////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////
////////////////////////Bluetooth Control (Passive)////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

  if(Serial.available() > 0){
   data1 = Serial.read();
   if(data1 == 'U'){        //U = Up
    p1++;
    q1++;
    r1++;
    s1++;
    lol++;
    thro_map++;
    Serial.print("The throttle map is:  ");
    Serial.println(thro_map);
   // delay(3000);
   }
   if(data1 == 'D'){        //D = Down
    p1--;
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif
  q1--;
    r1--;
    s1--;
    lol--;
    thro_map--;
    Serial.print("The throttle map is:  ");
    Serial.println(thro_map);
   // delay(3000);
   }
   if(data1 == 'F'){        //F = Front
    q1++;
    r1++;}
   if(data1 == 'B'){        //B = Back
    p1++;
    s1++;}
   if(data1 == 'L'){        //L = Left
    p1++;
    q1++;}
   if(data1 == 'R'){        //R = Right
    r1++;
    s1++;}
   if(data1 == 'E'){        //F = Front
    lol = 0;
    p1 = 0;
    q1 = 0;
    r1 = 0;
    s1 = 0;
    setState(STOPPED);
   }
}

throttle = 1000 + (thro_map*100);    //Standardising throttle variable

   if(lol > 10){
   p1 = 10;
   q1 = 10;
   r1 = 10;
   s1 = 10;}
   if(lol < 0){
   p1 = 0;
   q1 = 0;
   r1 = 0;
   s1 = 0;}

val1 = p1;
val2 = q1;
val3 = r1;
val4 = s1;

val1 = map(val1, 0, 10,1000,2000);  //mapping motor1 throttle to minimum and maximum(Change if needed) 
val2 = map(val2, 0, 10,1000,2000);  //mapping motor2 throttle to minimum and maximum(Change if needed) 
val3 = map(val3, 0, 10,1000,2000);  //mapping motor3 throttle to minimum and maximum(Change if needed) 
val4 = map(val4, 0, 10,1000,2000);  //mapping motor4 throttle to minimum and maximum(Change if needed) 


esc1.writeMicroseconds(val1);       //giving the signal to esc
esc2.writeMicroseconds(val2);
esc3.writeMicroseconds(val3); 
esc4.writeMicroseconds(val4);


if(data1 == 'F'){
    delay(300);
    q1--;
    r1--;
    val2 = q1;
    val3 = r1;
    val2 = map(val2, 0, 10,1000,2000); 
    val3 = map(val3, 0, 10,1000,2000);
    esc2.writeMicroseconds(val2);
    esc3.writeMicroseconds(val3);
   }
if(data1 == 'B'){
    delay(300);
    p1--;
    s1--;
    val1 = p1;
    val4 = s1;
    val1 = map(val1, 0, 10,1000,2000);
    val4 = map(val4, 0, 10,1000,2000);
    esc1.writeMicroseconds(val1);
    esc4.writeMicroseconds(val4);
   }
if(data1 == 'L'){
    delay(300);
    p1--;
    q1--;
    val1 = p1;
    val2 = q1;
    val1 = map(val1, 0, 10,1000,2000); 
    val2 = map(val2, 0, 10,1000,2000);
    esc1.writeMicroseconds(val1);
    esc2.writeMicroseconds(val2);
}
if(data1 == 'R'){
    delay(300);
    r1--;
    s1--;
    val3 = r1;
    val4 = s1;
    val3 = map(val3, 0, 10,1000,2000);
    val4 = map(val4, 0, 10,1000,2000);
    esc3.writeMicroseconds(val3); 
    esc4.writeMicroseconds(val4);
}
data1='Z';

/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////Active Control///////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

////////////// calculating ESC pulses necessary to level/////////////////////////////

  // calculate current orientation according to the gyro
  // use a 70-30 complementary filter
  // 65.5 is from the MPU-6050 spec

    Serial.print("gyro values are: ");
    Serial.print(y);Serial.print("\t");
    Serial.print(p);Serial.print("\t");
    Serial.print(r);Serial.println("\t"); 

  pid.yaw.gyro = (pid.yaw.gyro * 0.7) + ((y / 65.5) * 0.3);
  pid.roll.gyro = (pid.roll.gyro * 0.7) + ((r / 65.5) * 0.3);
  pid.pitch.gyro = (pid.pitch.gyro * 0.7) + ((p / 65.5) * 0.3);

 /*   Serial.print("\t pid YRP values:  ");
    Serial.print(pid.yaw.gyro);Serial.print("\t");
    Serial.print(pid.roll.gyro);Serial.print("\t");
    Serial.print(pid.pitch.gyro);Serial.print("\t"); */

  // calculate the angles
  // 0.0000611 = 1 / 250Hz / 65.5
  pitch_angle += p* 0.0000611;
  roll_angle += r * 0.0000611;

  // shift the roll/pitch axes if the drone has yawed
  pitch_angle -= roll_angle * sin(y * 0.000001066);
  roll_angle += pitch_angle * sin(y * 0.000001066);

  // calculate the total acceleration
  double total_acc = sqrt(pow2(ax) + pow2(ay) + pow2(az));
  if (abs(ay) < total_acc)
    pitch_acc = asin((float)ay/total_acc) * 57.296;
  if (abs(ax) < total_acc)
    roll_acc = -asin((float)ax/total_acc) * 57.296;

  // use acceleration to correct the drift from the gyro
  roll_angle = (roll_angle * 0.9996) + (roll_acc * 0.0004);
  pitch_angle = (pitch_angle * 0.9996) + (pitch_acc * 0.0004);
/*
    Serial.print("\t Angles are: ");
    Serial.print(roll_angle * 180/M_PI);Serial.print("\t");
    Serial.print(pitch_angle * 180/M_PI);Serial.print("\t");
*/
  double roll_adjust = roll_angle * 17.0;
  double pitch_adjust = pitch_angle * 17.0;

  // set PID targets
  pid.roll.target = 0;
  pid.roll.target -= roll_adjust;
  pid.roll.target /= 3;             // convert to degrees
/*    
    Serial.print("\t setpoints: ");
    Serial.print(pid.roll.target);Serial.print("\t");
*/
  pid.pitch.target = 0;
  pid.pitch.target -= pitch_adjust;
  pid.pitch.target /= 3;

//    Serial.print(pid.pitch.target);Serial.print("\t");
  
  pid.yaw.target = 0;
  
  // based on program state, do different things
  switch (state) {
  case STOPPED: {
   /* if (lol >= 2){*/setState(STARTING);//Serial.println("switched to STARTING MODE");
    resetESCPulses();
    //Serial.println("In STOPPED MODE");
    break;
  }
   
  case STARTING: {
    // do pre-flight setup
    pid.yaw.i.total = 0;
    pid.roll.i.total = 0;
    pid.pitch.i.total = 0;

    pid.yaw.d.prev = 0;
    pid.roll.d.prev = 0;
    pid.pitch.d.prev = 0;

    roll_angle = roll_acc;
    pitch_angle = pitch_acc;
    
    setState(STARTED);
//    Serial.println("switched to STARTED MODE");
    resetESCPulses();
    break;
  }

  case STARTED: {
    calculatePID(&pid);
    calculateESCPulses(throttle, &pid);
//    if (throttle < 1200){setState(STOPPED);
//    Serial.println("switched to STOPPED MODE");}
//    Serial.println("In STARTED MODE");
    break;
  }
  }

 /* // keep loop at 4000 microseconds (250 Hz) so ESC can function properly
  while (micros() - loop_timer < 4000);
  loop_timer = micros();

  PORTD |= 0b11110000; // turn on all the ESC
  unsigned long timer_1 = esc_fr + loop_timer;
  unsigned long timer_2 = esc_rr + loop_timer;
  unsigned long timer_3 = esc_rl + loop_timer;
  unsigned long timer_4 = esc_fl + loop_timer;
*/
  // there is always at least 1ms of free time, do something useful
//  readGyroValues(&gyro);

 /* while (PORTD & 0b11110000) {
    register unsigned long t = micros();
    if (timer_1 <= t) PORTD &= 0b11101111;
    if (timer_2 <= t) PORTD &= 0b11011111;
    if (timer_3 <= t) PORTD &= 0b10111111;
    if (timer_4 <= t) PORTD &= 0b01111111;
  }*/
}

inline void setState(State s) {
  state = s;
  }

inline void resetESCPulses() {
  esc_1 = 1000;
  esc_2 = 1000;
  esc_3 = 1000;
  esc_4 = 1000;
  }

inline void calculateESCPulses(int throttle, struct PID* pid) {
  // calculate ESC pulses necessary to level
    throttle = min(1800, throttle);                                                //Restricting the throttle to 1800us pulse
    esc_1 = throttle - pid->pitch.output + pid->roll.output - pid->yaw.output;
    esc_2 = throttle + pid->pitch.output + pid->roll.output + pid->yaw.output;
    esc_3 = throttle + pid->pitch.output - pid->roll.output - pid->yaw.output;
    esc_4 = throttle - pid->pitch.output - pid->roll.output + pid->yaw.output;

    if(esc_1 > 2000)esc_1 = 2000;                          //Limit the esc-1 pulse to 2000us.
    if(esc_2 > 2000)esc_2 = 2000;                          //Limit the esc-2 pulse to 2000us.
    if(esc_3 > 2000)esc_3 = 2000;                          //Limit the esc-3 pulse to 2000us.
    if(esc_4 > 2000)esc_4 = 2000;                          //Limit the esc-4 pulse to 2000us.

    if(esc_1 < 1100)esc_1 = 1100;                          //Limit the esc-1 pulse to 2000us.
    if(esc_2 < 1100)esc_2 = 1100;                          //Limit the esc-2 pulse to 2000us.
    if(esc_3 < 1100)esc_3 = 1100;                          //Limit the esc-3 pulse to 2000us.
    if(esc_4 < 1100)esc_4 = 1100;                          //Limit the esc-4 pulse to 2000us.    

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /*  Serial.print("Calculated ESC pulses are:  ");
    Serial.print("1 ");Serial.print(esc_1);Serial.print("\t");
    Serial.print("2 ");Serial.print(esc_2);Serial.print("\t");
    Serial.print("3 ");Serial.print(esc_3);Serial.print("\t");
    Serial.print("4 ");Serial.print(esc_4);Serial.println("\t");
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    esc1.writeMicroseconds(esc_1);                                                  //giving the signal to ESCs
    esc2.writeMicroseconds(esc_2);
    esc3.writeMicroseconds(esc_3); 
    esc4.writeMicroseconds(esc_4);
*/
    //delay(2000);
}
